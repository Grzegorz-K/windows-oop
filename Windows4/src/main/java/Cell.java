/**
 * Created by Grzegorz Kotyza on 19.05.2017.
 */
public class Cell {
    private int x;
    private int y;
    private Ball ball;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
