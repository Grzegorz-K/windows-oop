#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <fstream>
#include <QPainter>
#include <QStateMachine>
#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setMinimumSize(QSize(700,640));

    plot = new Plot(this->size());

    QStateMachine* stateMachine = new QStateMachine(this);

    QState* minState = new QState(stateMachine);
    QState* maxState = new QState(stateMachine);
    QState* normalState = new QState(stateMachine);



    minState->assignProperty(ui->actionDown,"enabled",false);
    minState->assignProperty(ui->actionUp,"enabled",true);

    normalState->assignProperty(ui->actionDown,"enabled",true);
    normalState->assignProperty(ui->actionUp,"enabled",true);

    maxState->assignProperty(ui->actionDown,"enabled",true);
    maxState->assignProperty(ui->actionUp,"enabled",false);

    minState->addTransition(plot, SIGNAL(normalLevel()), normalState);
    normalState->addTransition(plot, SIGNAL(minLevel()), minState);
    normalState->addTransition(plot, SIGNAL(maxLevel()), maxState);
    maxState->addTransition(plot, SIGNAL(normalLevel()), normalState);

    stateMachine->setInitialState(minState);
    stateMachine->start();




    connect(ui->actionUp,SIGNAL(triggered(bool)),plot,SLOT(up()));
    connect(ui->actionDown,SIGNAL(triggered(bool)),plot,SLOT(down()));
    connect(ui->actionClose,SIGNAL(triggered(bool)),this,SLOT(close()));
    connect(ui->actionOpen,SIGNAL(triggered(bool)),this,SLOT(openFile()));
    connect(ui->actioncopyImage,SIGNAL(triggered(bool)),this,SLOT(copyImage()));



}

MainWindow::~MainWindow()
{
    if(file)
        file->close();
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    plot->draw(painter);
    this->update();

}
void MainWindow::resizeEvent(QResizeEvent *)
{
    plot->setSize(this->size());
}
void MainWindow::openFile()
{
    QString fString = QFileDialog::getOpenFileName(this, "Open file","../FILES","*.txt");
    std::ifstream f;
    std::wstring fName = fString.toStdWString();
    f.open(fName,std::ifstream::in | std::ifstream::binary);
    if(!f.is_open())
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Error while opening file!");
        messageBox.setFixedSize(500,200);
        exit(1);

    }

    plot->lines.clear();

    char tmp[8];
    float x[125];
    float d, rirm, rfrm;
    for(int i=0; i<120;i++)
    {
        f.read(tmp,sizeof(float));
        memcpy(&plot->xx[i],&tmp,sizeof(float));
    }
    while(!f.eof())
    {


        f.read(tmp,sizeof(float));
        memcpy(&d,&tmp,sizeof(float));
        f.read(tmp,sizeof(float));
        memcpy(&rirm,&tmp,sizeof(float));
        f.read(tmp,sizeof(float));
        memcpy(&rfrm,&tmp,sizeof(float));
        for(int i=0; i<120;i++)
        {
            f.read(tmp,sizeof(float));
            memcpy(&x[i],&tmp,sizeof(float));

        }
        Line line(d,rirm,rfrm,x);
        plot->lines.push_back(line);
    }
}

void MainWindow::copyImage()
{
    QSize size = this->size();
    size.setHeight(size.height()-20);
    QPixmap pixmap(size);
    this->render(&pixmap,QPoint(0,-20));
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setPixmap(pixmap);

}

void MainWindow::on_actioncopyImage_triggered()
{
    copyImage();
}
