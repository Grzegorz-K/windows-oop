#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Plot.h"
#include <QListWidget>
#include <QVector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void openFile();

    void on_showButton_clicked();


private:
    Ui::MainWindow *ui;
    Plot* plot;
    float startVal;
    float stopVal;
    double nullVal;
    QVector<QVector<double>> data;
    QVector<double> yVector;
};

#endif // MAINWINDOW_H
