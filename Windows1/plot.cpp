#include "plot.h"
#include <QDebug>

Plot::Plot(QSize size)
{
    topLeft = QPoint(20,40);
    plotSize = size;
    level = 37;
    Dd=0;


}
void Plot::draw(QPainter &painter)
{
    QPoint mainPlot(topLeft.rx()+30,
            topLeft.ry()+20);
    QSize mainPlotSize(plotSize.width()-250,plotSize.height()-100);
    if(!lines.empty())
        Dd = lines[level*38].d;

    if(level*38 + 40 > lines.size())
        emit maxLevel();
    QString str = QString("D/d = %1").arg(Dd);
    painter.drawText(topLeft.rx()+40,topLeft.ry(),60,50,0,str);

    painter.drawRect(mainPlot.rx(),mainPlot.ry(),mainPlotSize.rwidth(),mainPlotSize.rheight());
    painter.drawLine(mainPlot.rx()+mainPlotSize.rwidth()/3,
                     mainPlot.ry(),
                     mainPlot.rx()+mainPlotSize.rwidth()/3,
                     mainPlot.ry()+mainPlotSize.rheight());
    painter.drawLine(mainPlot.rx()+2*mainPlotSize.rwidth()/3,
                     mainPlot.ry(),
                     mainPlot.rx()+2*mainPlotSize.rwidth()/3,
                     mainPlot.ry()+mainPlotSize.rheight());

    for(int i=1;i<5;i++)
    {
        painter.drawLine(mainPlot.rx(),
                         mainPlot.ry()+i*mainPlotSize.rheight()/5,
                         mainPlot.rx()+mainPlotSize.rwidth(),
                         mainPlot.ry()+i*mainPlotSize.rheight()/5);


     }
    QFont font = painter.font();

    painter.setFont(font);
    for(int i=0;i<=5;i++)
    {
        painter.drawText(mainPlot.rx()-25,mainPlot.ry()+i*mainPlotSize.rheight()/5-5,50,30,0,"10");
        font.setPointSize(6);
        painter.setFont(font);
        str = QString("%1").arg(4-i);
        painter.drawText(mainPlot.rx()-10,mainPlot.ry()+i*mainPlotSize.rheight()/5 -10,50,30,0,str);
        font.setPointSize(10);
        painter.setFont(font);
    }
    for(int i=0;i<4;i++)
    {
        font.setPointSize(10);
        painter.setFont(font);
        painter.drawText(mainPlot.rx()+i*mainPlotSize.rwidth()/3-10,
                         mainPlot.ry()+mainPlotSize.height()+5,
                         50,30,0,"10");
        str = QString("%1").arg(i-1);
        font.setPointSize(6);
        painter.setFont(font);
        painter.drawText(mainPlot.rx()+i*mainPlotSize.rwidth()/3+5,
                         mainPlot.ry()+mainPlotSize.height(),
                         50,30,0,str);

    }
    font.setPointSize(10);
    painter.setFont(font);
    static const QString lineVals[38] = {
        QString("0.10"),
        QString("0.20"),
        QString("0.30"),
        QString("0.40"),
        QString("0.50"),
        QString("0.75"),
        QString("1.00"),
        QString("1.25"),
        QString("1.50"),
        QString("1.75"),
        QString("2.00"),
        QString("2.50"),
        QString("3.00"),
        QString("4.00"),
        QString("5.00"),
        QString("7.50"),
        QString("10.0"),
        QString("15.0"),
        QString("20.0"),
        QString("25.0"),
        QString("30.0"),
        QString("40.0"),
        QString("50.0"),
        QString("75.0"),
        QString("100.0"),
        QString("150.0"),
        QString("200.0"),
        QString("250.0"),
        QString("300.0"),
        QString("400.0"),
        QString("500.0"),
        QString("750.0"),
        QString("1000.0"),
        QString("2000.0"),
        QString("3000.0"),
        QString("5000.0"),
        QString("7500.0"),
        QString("10000.0")

    };

    for(int i=37; i>=0; i--)
    {
        painter.fillRect(plotSize.width()-190 + (i%2 == 0)*100,
                     topLeft.ry()+10+15*i,
                     20,
                     10,
                     colors[37 - i]);
        painter.drawText(plotSize.width()-160 + (i%2 == 0)*100,
                        topLeft.ry()+8+15*i,
                         50,30,0,lineVals[37-i]);
    }

   QPen penHLines(QColor("#FF4444"));
    painter.setPen(penHLines);
    painter.drawText(plotSize.width()-130,
                    topLeft.ry()-10,
                     50,30,0,"Ri/Rm");
    painter.drawText(mainPlot.rx() - 50,
                    mainPlot.ry() + mainPlotSize.height()/2,
                     50,30,0,"Ra/Rm");
    painter.drawText(mainPlot.rx() + mainPlotSize.width()/2-5,
                    mainPlot.ry() + mainPlotSize.height()+15,
                     50,30,0,"L/d");

    penHLines.setColor(QColor("#C4C4C4"));
    painter.setPen(penHLines);
    //rysowanie siatki
    int sum =1;
    for(int j=1;j<=3;j++,sum=1)
    {
        for(int i=0;i<9;i++,sum +=i)
        {
            painter.drawLine(mainPlot.rx()+(mainPlotSize.rwidth()*j)/3 - (mainPlotSize.rwidth()*sum/55/3),
                             mainPlot.ry(),
                             mainPlot.rx()+(mainPlotSize.rwidth()*j)/3 - (mainPlotSize.rwidth()*sum/55/3),
                             mainPlot.ry()+mainPlotSize.rheight());
        }
    }
    sum =1;
    for(int j=1;j<=5;j++,sum=1)
    {
        for(int i=0;i<9;i++,sum +=i)
        {
            painter.drawLine(mainPlot.rx(),
                             mainPlot.ry()+ mainPlotSize.rheight()* sum/5/55 + ((j-1)*mainPlotSize.rheight())/5,
                             mainPlot.rx()+mainPlotSize.rwidth(),
                             mainPlot.ry()+mainPlotSize.rheight() * sum/5/55 + ((j-1)*mainPlotSize.rheight())/5);
        }
    }
    penHLines.setColor(colors[0]);
    painter.setPen(penHLines);
    float x0 = mainPlot.rx() + mainPlotSize.rwidth()/3;
    float y0 = mainPlot.ry() + 4*mainPlotSize.rheight()/5;
    QPointF points[120];
    if(lines.size())
     for(int k=0;k < 38; k++)
     {
         penHLines.setColor(colors[k]);
         painter.setPen(penHLines);
        for(int i=0; i<120;i++)
        {
            points[i] = QPointF(x0 + xx[i]*mainPlotSize.rwidth()/3,
                                y0 - lines[k+38*level].y[i]* mainPlotSize.rheight()/5);
        }
        painter.drawPolyline(points, 120);
     }




}
void Plot::setSize(QSize newSize)
{
    plotSize = newSize;
}
void Plot::up()
{

    level++;
    emit normalLevel();
}
void Plot::down()
{
    if(level > 37)
    {
        level--;
        if(level==37)
            emit minLevel();
        else emit normalLevel();
    }

}
