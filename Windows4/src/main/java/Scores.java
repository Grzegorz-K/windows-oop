/**
 * Created by Grzegorz Kotyza on 20.05.2017.
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Scores {
    private List<Integer> easy;
    private List<Integer> medium;
    private List<Integer> hard;

    public Scores() {
        load();
    }

    private void save() {
        try {
            FileOutputStream fileOut = new FileOutputStream("scores.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(easy);
            out.writeObject(medium);
            out.writeObject(hard);
            out.close();
            fileOut.close();

        }catch(IOException i) {
            i.printStackTrace();
        }
    }
    private void load() {
        try {
            FileInputStream fileIn = new FileInputStream("scores.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            easy = (ArrayList<Integer>) in.readObject();
            medium = (ArrayList<Integer>) in.readObject();
            hard = (ArrayList<Integer>) in.readObject();
            in.close();
            fileIn.close();
        }catch(IOException i) {
            i.printStackTrace();
        }catch(ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
        }
    }
    public void addScore(int score, int level) {
        List<Integer> list;
        if(level == 0) list = easy;
        else if(level == 1) list=medium;
        else list = hard;

        list.add(score);
        Collections.sort(list);
        Collections.reverse(list);
        if(list.size() > 10)
            list.remove(10);

        save();

    }
    public void printList(int level) {
        for(Integer item: easy) {
            System.out.println(item);

        }
    }
    public List<Integer> getScores(int level) {
        if(level == 0) return easy;
        if(level == 1) return medium;
        return hard;
    }


}
