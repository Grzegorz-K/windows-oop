import java.awt.*;

/**
 * Created by Grzegorz Kotyza on 19.05.2017.
 */
public class Data {
    private static Data ourInstance = new Data();
    public static final int cellCount = 9;
    public static final Color[] colors = {
            new Color(190, 0, 11),
            new Color(14, 52, 0),
            new Color(0, 1, 193),
            new Color(236, 222, 0),
            new Color(255, 140, 17),
            new Color(83, 235, 255),
            new Color(98, 0, 126),
            new Color(119, 117, 110),
            new Color(0, 224, 92),
    };

    public static Integer socre = 0;
    public static Data getInstance() {
        return ourInstance;
    }

    private Data() {
    }
}
