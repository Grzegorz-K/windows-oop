package com.company;

import java.awt.*;

import static java.lang.Math.abs;

/**
 * Created by Grzegorz Kotyza on 10.05.2017.
 */
public class Line {
    public Point p1, p2;
    Type type;
    public Color color;
    public enum Type {
        HORIZONTAL, VERTICAL;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    private float value = 0;

    public Line(Point p1, Point p2) {

        if(p1.y == p2.y) {
            type = Type.HORIZONTAL;
            if(p1.x < p2.x) {
                this.p1 = p1;
                this.p2 = p2;
            } else {
                this.p1 = p2;
                this.p2 = p1;
            }
        }
        else {
            type = Type.VERTICAL;
            if(p1.y < p2.y) {
                this.p1 = p1;
                this.p2 = p2;
            } else {
                this.p1 = p2;
                this.p2 = p1;
            }
        color = new Color(0,0,0);
        }
    }
    public boolean isPointInLine(Point x, int accuracy)
    {
        if(type==Type.HORIZONTAL && p1.x <= x.x && x.x <= p2.x && abs(p1.y - x.y) < accuracy)
            return true;
        if(type== Type.VERTICAL && p1.y <= x.y && x.y <= p2.y && abs(p1.x - x.x) < accuracy)
            return true;
        return false;
    }
    public void print() {
        System.out.println(p1 + " || " + p2);
    }
}
