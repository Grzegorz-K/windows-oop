#ifndef PLOT_H
#define PLOT_H

#include <QDialog>

namespace Ui {
class Plot;
}

class Plot : public QDialog
{
    Q_OBJECT

public:
    explicit Plot(QWidget *parent = 0, QVector<double> xPos = {0}, QVector<double> yPos = {0}, float start=0, float stop = 0, float nullVal=0);
    ~Plot();
    void print();

private slots:

    void on_verticalScrollBar_valueChanged(int value);

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::Plot *ui;
    float startVal;
    float stopVal;
    bool hasNegativeVlaues = false;
};

#endif // PLOT_H
