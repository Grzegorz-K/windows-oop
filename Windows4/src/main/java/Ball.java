import java.awt.*;

/**
 * Created by Grzegorz Kotyza on 19.05.2017.
 */
public class Ball {
    private Color color;
    private int x;
    private int y;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Ball(Color color, int pos_x, int pos_y) {

        this.color = color;
        this.x = pos_x;
        this.y = pos_y;
    }
    public Point getPos() {
        return new Point(x,y);
    }


}
