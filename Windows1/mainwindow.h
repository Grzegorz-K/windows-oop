#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include "plot.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Plot *plot;
    QFile *file;


private slots:
    void openFile();
    void copyImage();

    void on_actioncopyImage_triggered();

protected:

    void paintEvent(QPaintEvent* e);
    void resizeEvent(QResizeEvent * event);
};

#endif // MAINWINDOW_H
