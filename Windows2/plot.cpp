#include "plot.h"
#include "ui_plot.h"
#include <QDebug>
#include <QMessageBox>

Plot::Plot(QWidget *parent,  QVector<double> xPos,  QVector<double> yPos, float start, float stop, float nullVal) :
    QDialog(parent),
    ui(new Ui::Plot)
{
    ui->setupUi(this);
    startVal= start;
    stopVal = stop;

    QCustomPlot* plot =  ui->widgetPlot;
    plot->addGraph();


   double min = 9999999, max= -10;
   for(int i=0; i<xPos.size();i++)
   {
       if(xPos[i] == nullVal)
       {
           xPos.removeAt(i);
           yPos.removeAt(i);
           xPos.resize(xPos.size()-1);
           yPos.resize(yPos.size()-1);
           i--;
           continue;
       }
       if(xPos[i]< min && xPos[i] != nullVal)
           min = xPos[i];
       if(xPos[i] > max && xPos[i] != nullVal)
           max = xPos[i];
       if(xPos[i] < 0)
           hasNegativeVlaues = true;

   }
   qDebug() << min << max;


    //plot->graph(0)->setLineStyle(QCPGraph::lsStepLeft  );
     QCPCurve *curve = new QCPCurve(plot->xAxis, plot->yAxis);
     curve->addData(xPos,yPos);
    // give the axes some labels:
    plot->xAxis->setLabel("x");
    plot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    plot->xAxis->setRange(min , max);
    plot->yAxis->setRange(start, stop); 
    plot->replot();

    ui->verticalScrollBar->setRange(100, stop-start);
    ui->verticalScrollBar->setValue(stop-start);




}

Plot::~Plot()
{
    delete ui;
}

void Plot::on_verticalScrollBar_valueChanged(int value)
{
     ui->widgetPlot->yAxis->setRange(stopVal-value,stopVal-value+100);
     ui->widgetPlot->replot();

}

void Plot::on_comboBox_currentIndexChanged(int index)
{
    if(index)
    {
        if(hasNegativeVlaues)
        {
            QMessageBox::information(NULL, "Błąd", "Wykres posiada ujemne wartości - nie mozna zastosowac skali logarytmicznej!");
            this->ui->comboBox->setCurrentIndex(0);
        }
        else
            ui->widgetPlot->xAxis->setScaleType(QCPAxis::stLogarithmic);
    }
    else
        ui->widgetPlot->xAxis->setScaleType(QCPAxis::stLinear);

    ui->widgetPlot->replot();
}
void Plot::print()
{

}
