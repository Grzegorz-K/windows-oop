package com.company;

import static java.lang.Math.abs;

/**
 * Created by Grzegorz Kotyza on 12.05.2017.
 */
public class Potential {
     private float val;
     private float previousVal;
     public final boolean isBorder;
     public boolean done = false;

    public Potential(float val, boolean isBorder) {
        this.val = val;
        this.isBorder = isBorder;
        previousVal = val;
    }

    public float getVal() {
        return val;
    }

    public void setVal(float val) {
        this.val = val;
    }
    public float getPrevious() {
        return previousVal;
    }
    public float epsilon() {
        return abs(val - previousVal);
    }
    public void update() {
        previousVal=val;
    }
}
