package com.company;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Grzegorz Kotyza on 01.05.2017.
 */
public final class Points {
    private static volatile Points instance = null;
    private List<Integer> xList = new ArrayList<Integer>();
    private List<Integer> yList = new ArrayList<Integer>();
    public static final int accuracy = 15;

    public static Points getInstance() {
        if (instance == null) {
            synchronized (Points.class) {
                if (instance == null) {
                    instance = new Points();
                }
            }
        }
        return instance;
    }
    private Points() {

    }

    public Point addPoint(int x, int y){
        xList.add(x);
        yList.add(y);
        return new Point(x,y);

    }
    public Point addPoint(double x, double y) {
        return addPoint((int )x, (int) y);
    }
    public int[] getXTab() {
        int[] array = new int[xList.size()];
        for(int i = 0; i < xList.size(); i++)
            array[i] = xList.get(i);
        return array;
    }
    public int[] getYTab() {
        int[] array = new int[yList.size()];
        for(int i = 0; i < yList.size(); i++)
            array[i] = yList.get(i);
        return array;
    }
    public int getPointsCount() {
        return xList.size();
    }

    public Point getTopLeft() {
        int top_x = 999;
        int top_y= 999;
        for(int i = 0; i < xList.size(); i++)
        {
            if(xList.get(i) < top_x)
                top_x = xList.get(i);
            if (yList.get(i) < top_y)
                top_y = yList.get(i);
        }
        return new Point(top_x, top_y);

    }
    public Point getBottomRight() {
        int top_x = 0;
        int top_y= 0;
        for(int i = 0; i < xList.size(); i++)
        {
            if(xList.get(i) > top_x )
                top_x = xList.get(i);
            if( yList.get(i) > top_y)
                top_y = yList.get(i);
        }
        return new Point(top_x, top_y);

    }
    public Point getAt(int i) {
        if(xList.size() == 0 || i>xList.size())
            return null;

        return new Point(xList.get(i),yList.get(i));
    }
    public void clear() {
        xList.clear();
        yList.clear();
    }
    public int getSize() {
       return xList.size();
    }

}
