#ifndef LINE_H
#define LINE_H
#include <iostream>

class Line
{
public:
    float d;
    float RiRm;
    float RfRm;
    float y[120];

public:
    Line(float pD, float pRiRm, float pRfRm, float* pY): d(pD), RiRm(pRiRm), RfRm(pRfRm)
    {
        memcpy(y,pY,sizeof(float)*120);
    }

};

#endif // LINE_H
