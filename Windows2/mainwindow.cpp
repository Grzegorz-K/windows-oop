#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QDebug>
//#include <QPrintDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionOpen,SIGNAL(triggered(bool)),this,SLOT(openFile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::openFile()
{
    QString fString = QFileDialog::getOpenFileName(this, "Open file","../FILES","*.las");
    QFile file(fString);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    QString line = in.readLine();
    this->ui->listWidget->clear();
    data.clear();
    yVector.clear();

    int currentPos = 0;
    int dataBeginPos = INT_MAX;
    int dataLine = 0;
    double tmp;

    while (!line.isNull())
    {
        //process_line(line);
        line = in.readLine();
        if(line.contains("STRT.M"))
        {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            words[1].remove(':');
            startVal = words[1].toFloat();
        }
        else if(line.contains("STOP.M"))
        {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            words[1].remove(':');
            stopVal = words[1].toFloat();
        }
        else if(line.contains("NULL."))
        {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            words[1].remove(':');
            nullVal = words[1].toDouble();
        }
        else if(line.contains("~A"))
        {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            words.removeFirst();
            words.removeFirst();

           this->ui->listWidget->addItems(words);
           this->ui->showButton->setEnabled(true);
            dataBeginPos = currentPos;
        }
        else if(line.contains(QRegExp("[0-9]")) && currentPos >= dataBeginPos)
        {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            yVector.push_back(words[0].toDouble());

           if(data.size()==0)
           {
               data.resize(words.size()+1);
           }
            for(int i=1;i<words.size();i++)
            {
                tmp = words[i].toDouble();
                data[dataLine++].push_back(tmp);
            }
             dataLine=0;
        }
        currentPos++;
    }
    this->ui->listWidget->setEnabled(true);

}

void MainWindow::on_showButton_clicked()
{
    int listItemPos = this->ui->listWidget->currentRow();
    qDebug() << listItemPos;
    plot = new Plot(this, data[listItemPos],yVector,startVal,stopVal,nullVal);
    plot->exec();
    delete plot;
}

