import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

/**
 * Created by Grzegorz Kotyza on 18.05.2017.
 */
public class Main extends JFrame{

    private JPanel content;
    private JTextField scoreField;
    private JComboBox difficultyBox;
    private JButton playButton;
    private JButton highScoresButton;
    private Board board;
    private Data data;
    private int level;
    private Ball selectedBall;
    private Scores scores;

    private GameState state;
    Random generator = new Random();


    Main() {

        setContentPane(content);
        setSize(700,620);
        setResizable(false);
        setLocation(400,150);

        board.setBackground(Color.WHITE);
        data = Data.getInstance();
        state = GameState.START;
        scoreField.setText(Data.socre.toString());
        scores = new Scores();



        setTitle("Kulki");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        playButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if (state == GameState.START) {
                    board.clear();
                    int ind = difficultyBox.getSelectedIndex();
                    if(ind == 0)
                        level = 5;
                    else if(ind == 1)
                        level = 7;
                    else
                        level = 9;

                    state = GameState.PLAY;
                    playButton.setText("Reset");
                    difficultyBox.setEnabled(false);
                    generateBalls(3);
                } else if(state == GameState.PLAY || state == GameState.BALL_SELECTED) {
                    finishGame();
                }

            }
        });

        board.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseClicked(e);
                if(e.getButton() == MouseEvent.BUTTON1) {

                    int x = e.getX()/60;
                    int y = e.getY()/60;
                    if(x<board.getWidth() && y<board.getHeight()) {
                        if(state == GameState.PLAY) {
                            selectedBall = board.getBall(x,y);
                            if(selectedBall != null)
                                state = GameState.BALL_SELECTED;

                        }
                        else if(state == GameState.BALL_SELECTED) {
                            if(board.isCellFree(x,y)) {
                                int res = board.moveBall(selectedBall,x,y);
                                if(res ==0) {
                                    if (!generateBalls(3))
                                        return;
                                    state = GameState.PLAY;

                                }
                                scoreField.setText(Data.socre.toString());
                            } else {
                                selectedBall = board.getBall(x,y);
                            }
                        }
                    }
                }
            }
        });
        highScoresButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

               new ScoreGUI(content);
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    private void createUIComponents() {
        board = Board.getInstance();
    }

    private void finishGame() {
        JOptionPane.showMessageDialog(content,"Game over! Your score: " + Data.socre,"Kulki",JOptionPane.INFORMATION_MESSAGE);
        state = GameState.START;
        playButton.setText("Play!");
        difficultyBox.setEnabled(true);
        board.clear();
        scores.addScore(Data.socre,difficultyBox.getSelectedIndex());
        Data.socre = 0;
    }
    private boolean generateBalls(int count) {
        for(int i = 0; i < count; i++) {
            Point p = board.getFreeCell();
            if(p==null) {
                finishGame();
                return false;
            }
            board.addBall(Data.colors[generator.nextInt(level)], p.x, p.y);
            if(board.getFreeCellCount() == 0) {
                finishGame();
                return false;
            }
        }
        return true;
    }
}


