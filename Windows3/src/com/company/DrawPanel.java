package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static com.company.MainDialog.tabHeight;

/**
 * Created by Grzegorz Kotyza on 30.04.2017.
 */

class DrawPanel extends JPanel{
        private Graphics g;
        private Points points;
        Color black = new Color(0,0,0);

        public DrawPanel() {
            super.setPreferredSize(new Dimension(800, 600));
            points = Points.getInstance();
            g=getGraphics();
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
        }
        public void drawLines()
        {
            g = getGraphics();

            g.clearRect(0,0,800,600);
            setBackground(new java.awt.Color(255, 255, 255));
            paintComponent(g);
            g.drawPolyline(points.getXTab(),points.getYTab(),points.getPointsCount());

            Graphics2D gg = (Graphics2D) g;
            gg.setColor(Color.GRAY);
            Point begin = points.getAt(0);
            gg.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{4}, 0));
            gg.drawLine(0,begin.y,this.getWidth(),begin.y);
            gg.drawLine(begin.x,0,begin.x,this.getHeight());



        }
        public void drawFigure() {
            g = getGraphics();

            g.clearRect(0,0,800,600);
            setBackground(new java.awt.Color(255, 255, 255));
            paintComponent(g);
            List<Line> lines = MainDialog.lines;
            for(int i = 0; i< lines.size(); i++) {
                g.setColor(lines.get(i).color);
                g.drawLine(lines.get(i).p1.x,lines.get(i).p1.y,lines.get(i).p2.x,lines.get(i).p2.y);
                g.setColor(Color.black);
            }

        }
        public void colorPoints(Point top, Potential[][] potentials) {
            g = getGraphics();
            float tab[] = new float[12];
            float diff = MainDialog.maxValue - MainDialog.minValue;
            for(int i=1; i<13; i++)
            {
                tab[i-1] =  MainDialog.minValue +  diff*(i)/(12);
            }

            Color colors[] = new Color[10];
            int col=0;
            colors[col++] = new Color(0, 77, 26);
            colors[col++] = new Color(0, 133, 11);
            colors[col++] = new Color(46, 226, 0);
            colors[col++] = new Color(134, 251, 0);
            colors[col++] = new Color(173, 255, 0);
            colors[col++] = new Color(255, 255, 0);
            colors[col++] = new Color(255, 205, 0);
            colors[col++] = new Color(255, 104, 0);
            colors[col++] = new Color(234, 36, 0);
            colors[col++] = new Color(255, 1, 0);



            for(int i = 0; i < MainDialog.tabWidth; i++) {
                for (int j = 0; j < tabHeight; j++) {
                    Point curr = new Point(top.x+i,top.y+j);
                    Potential pot = potentials[i][j];
                    boolean found = false;
                    if(!pot.isBorder) {
                        for(int k=9; k >= 0; k--) {
                            if(pot.getVal() >= tab[k]) {
                                g.setColor(colors[k]);
                                found = true;
                                break;
                            }
                            if(!found) {
                                found = true;
                                g.setColor(colors[0]);
                            }
                        }
                        g.drawLine(curr.x, curr.y, curr.x, curr.y);
                    }

                }
            }
            g.setColor(Color.white);
            g.fillRect(680,0,100,600);
            for(int i=0; i <10; i++) {
                g.setColor(colors[9-i]);
                g.fillRect(720,40+40*i,40,40);
                g.setColor(Color.black);
                g.drawString(String.format("%.2f",tab[12-i-1]),680,45+40*i);

            }

        }


        public void clear() {
            points.clear();
            g = getGraphics();
            g.clearRect(0,0,800,600);
            this.setBackground(new java.awt.Color(255, 255, 255));
            paintComponent(g);
        }
    }


