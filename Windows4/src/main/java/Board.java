import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Grzegorz Kotyza on 18.05.2017.
 */
public class Board extends JPanel{

    private static Board ourInstance = new Board();
    static Ball [][] cells;
    Data data;
    private  Graphics g;
    public List <Point> freeCells;

    private Board() {
        cells = new Ball[Data.cellCount][Data.cellCount];
        data = Data.getInstance();
        freeCells = new ArrayList<Point>();

    }

    public static Board getInstance() {
        return ourInstance;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
            drawGrid(g);
        }


    public boolean addBall(Color c, int px, int py) {
        cells[px][py] = new Ball(c,px,py);
        freeCells.remove(new Point(px,py));
        boolean success = check(px, py);
        update();
        return success;
    }

    public void drawGrid(Graphics g) {

        int width = getWidth();
        int height = getHeight();
        g.drawLine(0,0,width,0);
        g.drawLine(0,0,0,width);
        g.drawLine(width-1,0,width-1,height);
        g.drawLine(0,height-1,width-1,height-1);
        for(int i =1; i < 10;i++) {
            g.drawLine(i * width / 9, 0, i * width / 9, height);
            g.drawLine(0, i * height / 9, width, i * height / 9);

        }

    }
    public void update() {
        g= getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0,0,getWidth(),getHeight());
        g.setColor(Color.black);
        drawGrid(g);
        Ball b;
        for(int i = 0; i < Data.cellCount; i++) {
            for(int j=0; j <Data.cellCount; j++)
                if((b =cells[i][j]) != null) {
                    g.setColor(b.getColor());
                    g.fillOval(61*i+5,61*j+5,50,50);
                }
        }
    }

    public void clear() {
        freeCells.clear();
        for (int i = 0; i < Data.cellCount; i++) {
            for (int j = 0; j < Data.cellCount; j++) {
                cells[i][j] = null;
                freeCells.add(new Point(i,j));
            }
        }
        update();
    }
    public Point getFreeCell() {
        if(freeCells.size() == 0)
            return null;
        int ind = new Random().nextInt(freeCells.size());
        Point x = freeCells.get(ind);
        freeCells.remove(ind);
        return x;
    }
    public int getFreeCellCount() {
        return freeCells.size();
    }
    public boolean isCellFree(int x, int y) {
        return cells[x][y] == null;
    }
    public Ball getBall(int x, int y) {
        return cells[x][y];
    }
    public int moveBall(Ball b, int newX, int newY) {
        Point pos = b.getPos();
        if(isPathAvailable(pos.x,pos.y,newX,newY)) {
            freeCell(pos.x,pos.y);
            if(addBall(b.getColor(), newX, newY))
            {
                update();
                return 1;
            }

            update();
            return 0;
        }
        return -1;
    }
    public boolean check(int startX, int startY) {
        int size = Data.cellCount;
        List<Point> matching = new ArrayList<Point>();
        Ball b = getBall(startX,startY);
        int i  = startX;
        int j;
        while(i >= 0 && cells[i][startY] != null && cells[i][startY].getColor().equals(b.getColor())) {
            matching.add(new Point(i,startY));
            i--;
        }
        i = startX + 1;
        while(i < size && cells[i][startY] != null && cells[i][startY].getColor().equals(b.getColor())) {
            matching.add(new Point(i,startY));
            i++;
        }
        if(matching.size() > 4) {
            removeMatching(matching);
            return true;
        }
        matching.clear();
        i = startY;
        while(i >= 0 && cells[startX][i] != null && cells[startX][i].getColor().equals(b.getColor())) {
            matching.add(new Point(startX,i));
            i--;
        }
        i = startY + 1;
        while(i < size && cells[startX][i] != null && cells[startX][i].getColor().equals(b.getColor())) {
            matching.add(new Point(startX,i));
            i++;
        }
        if(matching.size() > 4) {
            removeMatching(matching);
            return true;
        }
        matching.clear();

        for(i=startX, j = startY;i>=0 && j >=0 && cells[i][j] != null && cells[i][j].getColor().equals(b.getColor());i--,j--) {
            matching.add(new Point(i,j));
        }
        for(i=startX+1, j = startY+1;i<size && j<size && cells[i][j] != null && cells[i][j].getColor().equals(b.getColor());i++,j++) {
            matching.add(new Point(i,j));
        }
        if(matching.size() > 4) {
            removeMatching(matching);
            return true;
        }
        matching.clear();

        for(i=startX, j = startY;i<size && j>=0 && cells[i][j] != null && cells[i][j].getColor().equals(b.getColor());i++,j--) {
            matching.add(new Point(i,j));
        }
        for(i=startX-1, j = startY+1;i>=0 && j<size && cells[i][j] != null && cells[i][j].getColor().equals(b.getColor());i--,j++) {
            matching.add(new Point(i,j));
        }
        if(matching.size() > 4) {
            removeMatching(matching);
            return true;
        }
        matching.clear();
        matching.add(new Point(startX,startY));
        if(compare(startX,startY,startX+1,startY) && compare(startX+1,startY,startX+1,startY+1) &&
                compare(startX+1,startY+1,startX,startY+1)) {
            matching.add(new Point(startX+1,startY));
            matching.add(new Point(startX+1,startY+1));
            matching.add(new Point(startX,startY+1));
            removeMatching(matching);
            return true;
        }
        if(compare(startX,startY,startX-1,startY) && compare(startX-1,startY,startX-1,startY-1) &&
                compare(startX-1,startY-1,startX,startY-1)) {
            matching.add(new Point(startX-1,startY));
            matching.add(new Point(startX-1,startY-1));
            matching.add(new Point(startX,startY-1));
            removeMatching(matching);
            return true;
        }
        if(compare(startX,startY,startX,startY+1) && compare(startX,startY+1,startX-1,startY+1) &&
                compare(startX-1,startY+1,startX-1,startY)) {
            matching.add(new Point(startX,startY+1));
            matching.add(new Point(startX-1,startY+1));
            matching.add(new Point(startX-1,startY));
            removeMatching(matching);
            return true;
        }
        if(compare(startX,startY,startX,startY-1) && compare(startX,startY-1,startX+1,startY-1) &&
                compare(startX+1,startY-1,startX+1,startY)) {
            matching.add(new Point(startX,startY-1));
            matching.add(new Point(startX+1,startY-1));
            matching.add(new Point(startX+1,startY));
            removeMatching(matching);
            return true;
        }

        return false;

    }
    private boolean compare(int x1, int y1, int x2, int y2) {
        if(x1 < 0 || x1 >= Data.cellCount || y1 < 0 || y1 >= Data.cellCount ||x2 < 0 || x2 >= Data.cellCount || y2 < 0 || y2 >= Data.cellCount )
            return false;
        if(cells[x1][y1] == null || cells[x2][y2] == null)
            return false;
        return cells[x1][y1].getColor().equals(cells[x2][y2].getColor());
    }
    public boolean isPathAvailable(int x1, int y1, int x2, int y2) {
        int current = 1;
        Point searched = new Point(x2,y2);
        List<Point> available;
        List<Point> prevAvailable = new ArrayList<Point>();
        prevAvailable.add(new Point(x1,y1));
        boolean[][] used = new boolean[Data.cellCount][Data.cellCount];
        used[x1][y1] = true;

        Point p;
        while (current > 0) {
            available = new ArrayList<Point>();
            for(int i = 0; i < prevAvailable.size(); i++) {

                p=prevAvailable.get(i);
                if(p.x -1 >= 0 && !used[p.x-1][p.y] && cells[p.x-1][p.y] == null) {
                    available.add(new Point(p.x-1,p.y));
                    used[p.x-1][p.y] = true;
                }
                if(p.x + 1 < Data.cellCount  && !used[p.x+1][p.y] && cells[p.x+1][p.y] == null) {
                    available.add(new Point(p.x+1,p.y));
                    used[p.x+1][p.y] = true;
                }
                if(p.y -1 >= 0 && !used[p.x][p.y-1] && cells[p.x][p.y-1] == null) {
                    available.add(new Point(p.x,p.y-1));
                    used[p.x][p.y-1] = true;
                }
                if(p.y + 1 < Data.cellCount  && !used[p.x][p.y+1] && cells[p.x][p.y+1] == null) {
                    available.add(new Point(p.x,p.y+1));
                    used[p.x][p.y+1] = true;
                }


            }
            if(available.indexOf(searched) != -1)
                return true;
            current = available.size();
            prevAvailable.clear();
            prevAvailable = available;

        }

        return false;
    }
    private void removeMatching(List<Point> matching) {
        Data.socre += matching.size();
        for(Point p : matching) {
            freeCell(p.x,p.y);
        }
    }
    private void freeCell(int x, int y) {
        cells[x][y] = null;
        freeCells.add(new Point(x,y));
    }

}

