package com.company;

import com.sun.java.swing.plaf.motif.MotifTextUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class MainDialog extends JFrame {
    private JPanel contentPane;
    private JComboBox actionBox;
    private JButton generateButton;
    private JButton resetButton;
    private JTextField betaField;
    private JTextField epsField;
    private JProgressBar progressBar1;
    private DrawPanel drawPanel;
    private int mode = 0;
    private Points points;
    private Point lastPoint;
    private Point firstPoint;
    private Potential[][] potentials;
    public static int tabWidth;
    public static int tabHeight;
    public static int pointsCount;
    public static int finishedCount;
    public static float minValue;
    public static float maxValue;

    public float beta;
    public float eps;
    private int iter;
    private int accuracy = 15;
    public static List<Line> lines = new ArrayList<Line>();

    public MainDialog() {
        setContentPane(contentPane);
        setLocation(600, 200);
        setSize(800, 600);
        points = Points.getInstance();

        setResizable(false);
        setTitle("Laplace");
        //setVisible(true);
        drawPanel.setBackground(new java.awt.Color(255, 255, 255));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        drawPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if(e.getButton() == MouseEvent.BUTTON1)
                {
                    if(mode == 0) {
                        Point x = new Point(e.getX(), e.getY());

                        if(lastPoint != null) {
                            if(abs(lastPoint.getY()-x.getY()) < accuracy) {

                                if(abs(e.getX()-firstPoint.getX())< accuracy) {
                                    lastPoint = points.addPoint(firstPoint.getX(), lastPoint.getY());
                                } else
                                    lastPoint = points.addPoint(e.getX(), lastPoint.getY());
                            } else if(abs(lastPoint.getX()-x.getX()) < accuracy) {
                                if(abs(e.getY()-firstPoint.getY())< accuracy) {
                                    lastPoint = points.addPoint(lastPoint.getX(), firstPoint.getY());
                                } else
                                    lastPoint = points.addPoint(lastPoint.getX(), e.getY());
                            }
                            if(points.getSize() > 3 && lastPoint.equals(firstPoint))
                                setMode(1);

                        } else
                        {
                            points.addPoint(e.getX(),e.getY());
                            lastPoint = x;
                            firstPoint = x;
                        }
                        drawPanel.drawLines();
                    } else {
                        for(int i = 0; i< lines.size(); i++) {
                            if(lines.get(i).isPointInLine(new Point(e.getX(),e.getY()),Points.accuracy)) {
                                lines.get(i).color = new Color(0, 0, 168);
                                String val = JOptionPane.showInputDialog(contentPane, "Insert value:");
                                if(!val.isEmpty())
                                    lines.get(i).setValue(Float.parseFloat(val));
                                else
                                    lines.get(i).setValue(0);
                                break;
                            }
                        }
                        drawPanel.drawFigure();
                    }

                }

            }
        });
        actionBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode = actionBox.getSelectedIndex();
            }
        });
        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(betaField.getText().isEmpty() || epsField.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Inserta beta and eps first!", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                eps = Float.parseFloat(epsField.getText());
                beta = Float.parseFloat(betaField.getText());
                if(lines.size() == 0) {
                    JOptionPane.showMessageDialog(null, "Draw figure first!", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                createData();
            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                drawPanel.setBackground(new java.awt.Color(255, 255, 255));
                drawPanel.clear();
                lines.clear();
                pointsCount = 0;
                finishedCount = 0;
                lastPoint = firstPoint = null;
                setMode(0);
            }
        });
    }
    private void setMode(int m) {
        mode = m;
        actionBox.setSelectedIndex(m);
        for(int i=1; i <points.getSize(); i++) {
            lines.add(new Line(points.getAt(i - 1), points.getAt(i)));
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainDialog().setVisible(true);
            }
        });

    }
    private void createData() {
        Point top = points.getTopLeft();
        Point bottom = points.getBottomRight();
        tabWidth = bottom.x-top.x;
        tabHeight = bottom.y-top.y;
        pointsCount = 0;
        finishedCount = 0;
        iter = 0;
        Polygon polygon = new Polygon(points.getXTab(),points.getYTab(),points.getSize());
        potentials = new Potential[tabWidth+1][tabHeight+1];
        for(int i = 0; i <=tabWidth; i++) {
            for(int j = 0; j<= tabHeight; j++) {
                Point curr = new Point(top.x+i,top.y+j);
                Line line = isPointInBorder(curr);
                if(line != null) {
                    potentials[i][j] = new Potential(line.getValue(),true);
                    continue;
                }
                else if(polygon.contains(curr))
                {
                    pointsCount++;
                    potentials[i][j] = new Potential(0,false);
                    continue;
                }
                potentials[i][j] = new Potential(-1,true);;
            }
        }
        progressBar1.setMinimum(0);
        progressBar1.setMaximum(pointsCount);
        progressBar1.setValue(0);

       // drawPanel.colorPoints(top,potentials);

        while (finishedCount < pointsCount) {
            finishedCount = 0;
            for(int i = 0; i <tabWidth ; i++) {
                for(int j = 0; j< tabHeight ; j++) {
                    Potential pot = potentials[i][j];
                    if(!pot.isBorder) {
                       pot.setVal(((1-beta)*pot.getVal()) + beta*((potentials[i + 1][j].getPrevious() + potentials[i - 1][j].getVal() + potentials[i][j + 1].getPrevious() + potentials[i][j - 1].getVal()) / 4));
                    }

                }

            }
//            getMinMaxV();
//            drawPanel.colorPoints(top,potentials);
            for(int i = 0; i <tabWidth ; i++) {
                for (int j = 0; j < tabHeight; j++) {
                   // System.out.println(potentials[i][j].epsilon());
                    if(potentials[i][j].epsilon() < eps && !potentials[i][j].isBorder)
                        finishedCount++;
                    potentials[i][j].update();
                }
            }
            iter++;
            progressBar1.setValue(finishedCount);
            progressBar1.update(progressBar1.getGraphics());

        }
        System.out.println("koniec " + pointsCount + " iterations: " + iter );

        getMinMaxV();
        drawPanel.colorPoints(top,potentials);
        JOptionPane.showMessageDialog(contentPane,"Finished in " + iter + " iterations.\nMin: " + minValue + "\n max: " + maxValue);
    }
    private Line isPointInBorder(Point p) {
        for(Line line:lines) {
            if(line.isPointInLine(p,1))
            {
                return line;
            }
        }

        return null;
    }
    private void getMinMaxV() {
        int min_i = -1 , min_j = -1 , max_i = -1, max_j = -1;
        float minVal = 999, maxVal = -999;
        for(int i = 0; i <tabWidth -1; i++) {
            for (int j = 0; j < tabHeight - 1; j++) {
                Potential curr = potentials[i][j];
                if (!curr.isBorder && curr.getVal() !=-1) {
                    if(curr.getVal() < minVal) {
                        minVal = curr.getVal();
                        min_i = i;
                        min_j = j;
                    }
                    if(curr.getVal() > maxVal) {
                        maxVal = curr.getVal();
                        max_i = i;
                        max_j = j;
                    }
                }
            }
        }
        minValue = minVal;
        maxValue = maxVal;
    }

}
