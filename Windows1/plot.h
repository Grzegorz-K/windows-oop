#ifndef PLOT_H
#define PLOT_H

#include <QtCore>
#include <QtGui>
#include <QPainter>
#include <QDebug>
#include "memory"
#include "line.h"



class Plot: public QObject
{
    Q_OBJECT

public:
    Plot(QSize size);
    void draw(QPainter &painter);
    void setSize(QSize newSize);
    float xx[120];
    std::vector<Line> lines;
    QColor colors[38] = {
                QColor(18, 140, 207),
                QColor(2, 31, 10),
                QColor(77, 89, 121),
                QColor(248, 27, 220),
                QColor(10, 55, 143),
                QColor(154, 24, 131),
                QColor(45, 4, 44),
                QColor(143, 12, 81),
                QColor(28, 235, 196),
                QColor(58, 67, 15),
                QColor(110, 246, 119),
                QColor(242, 162, 62),
                QColor(16, 12, 119),
                QColor(73, 231, 86),
                QColor(134, 207, 83),
                QColor(72, 230, 10),
                QColor(45, 217, 47),
                QColor(175, 204, 129),
                QColor(123, 249, 9),
                QColor(57, 95, 157),
                QColor(59, 121, 245),
                QColor(214, 59, 56),
                QColor(213, 89, 97),
                QColor(141, 22, 97),
                QColor(44, 11, 251),
                QColor(163, 2, 161),
                QColor(157, 89, 162),
                QColor(14, 146, 59),
                QColor(205, 83, 73),
                QColor(128, 86, 103),
                QColor(90, 225, 148),
                QColor(194, 220, 149),
                QColor(96, 93, 104),
                QColor(85, 17, 176),
                QColor(21, 88, 128),
                QColor(178, 123, 87),
                QColor(5, 219, 179),
                QColor(137, 118, 36)
            };

private:
    QPoint topLeft;
    QSize plotSize;
    int level;
    float Dd;
    void drawGrid(QPainter &painter);

public slots:
    void up();
    void down();


signals:
    void minLevel();
    void maxLevel();
    void normalLevel();

};

#endif // PLOT_H
