import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by Grzegorz Kotyza on 20.05.2017.
 */
public class ScoreGUI extends JFrame{
    private JPanel view;
    private JList listEasy;
    private JList listHard;
    private JList listMedium;
    private JButton OKButton;
    private Scores scores = new Scores();

    public ScoreGUI(Component parent) {
        setLocationRelativeTo(parent);
        setContentPane(view);
        setSize(600,400);
        List<Integer> easy= scores.getScores(0);
        List<Integer> medium= scores.getScores(1);
        List<Integer> hard= scores.getScores(2);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        listEasy.setListData(easy.toArray());
        listMedium.setListData(medium.toArray());
        listHard.setListData(hard.toArray());
        setVisible(true);


        OKButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}
